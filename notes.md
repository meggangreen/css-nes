### Model
![model](model.png)

### Structure
- console
  - top
    - base
      - door
        - text
          - logo
          - superscript
          - large
        - handle
      - band
        - spacer
  - bottom
    - base - how to make triangles? like in m.g?
      - shadow
      - power container
        - light
          - cover
          - glow
            - off
            - on
        - button container x 2
          - button x 2 - for a11y, needs to be `<button>`
            - text
              - small
      - band
        - plug area
          - plug container
            - text -> top: -Npx;
            - plug
              - hole x 7

### Font
- Logo:   https://www.fontspace.com/nintend-font-f29918
- other:  https://www.fontspace.com/need-every-sound-font-f29917
  - download
  - convert to web font 
      (https://andrewsun.com/tools/woffer-woff-font-converter/)
  - upload and incorporate
      (https://medialoot.com/blog/how-to-use-web-fonts/)
      (https://www.w3schools.com/css/css3_fonts.asp)

### Colors
- Basics (more complex gradients follow)
  - text red: `#BE2736`
  - band: `#191919`
  - base top: `#D9D9D9`
  - base bottom: `#969696`

### Triangles from m.g
- could do two shapes, a rectangle plus trapezoid
  - placing some divs inside the base div would make it okay
  - box shadow mimics div's shape, not the trapezoid's
- could use `transform` with `perspective` and `rotate`
  - this would be a lot of guess work and sometimes doesn't look even
  - but the box shadow mimics the visible shape
- https://stackoverflow.com/questions/7920754/how-to-draw-a-trapezium-trapezoid-with-css3
 - i think i might have liked `shape-outside`, but i don't want to do it over
   (https://blog.logrocket.com/its-a-trap-ezoid-css-shapes-aren-t-what-you-d-expect-fe27a210001e/)
