![Illustration of Nintendo Entertainment System console](finished.png)
![1980s ad for the "ultimate Nintendo bundle"](ad-ultimate.jpeg)
![1980s ad for the coveted Nintendo Power Glove](ad-powerglove.jpg)
![Photo of NES console](model.png)